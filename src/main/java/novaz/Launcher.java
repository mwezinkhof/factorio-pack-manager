/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz;

import novaz.tools.Log;
import novaz.tools.LogLevel;
import novaz.ui.OptionsPanel;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class Launcher {
	public static void main(String... args) throws Exception {
		boolean gui = true;
		Config.init();
		for (String arg : args) {
			if (arg.contains("=")) {
				String[] override = arg.split("=");
				if (override[0].equalsIgnoreCase("pack")) {
					Config.lastPack = override[1].trim();
				}
			}
			if (arg.equals("-verbose")) {
				Config.loglevel = LogLevel.DEBUG;
			}
			if (arg.equals("nogui")) {
				gui = false;
			}
		}
		File f = new File(Config.gameLocation + "/bin/x64/factorio.exe");
		if (!f.exists()) {
			Log.debug("64 bit not found, trying 32 bit!");
			f = new File(Config.gameLocation + "/bin/Win32/factorio.exe");
		}
		Config.saveProperties();
		if (!gui) {
			if (f.exists() && !Config.lastPack.equals("")) {
				try {
					ModPack mp = new ModPack(Config.lastPack);
					mp.updateSymbolicLinks();
				} catch (Exception e) {
					Log.fatal("failed to create pack");
					Log.fatal(e);
				}
				try {
					Launcher.runFactorio(f.getAbsolutePath());
					Log.info("skipping for now");
				} catch (Exception e) {
					Log.fatal(e);
				}
			} else {
				if (!f.exists()) {
					Log.fatal("Couldn't find the factorio instance. To manually change this, change the game_location value in the config file.");
				}
				if (Config.lastPack.equals("")) {
					Log.fatal("No pack/instance specified. To do this change this, start the jar file with 'pack=packname' as argument");
				}
			}
		} else {
			final File executeable = f;
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					new OptionsPanel(null, Config.packLocation, executeable).setVisible(true);
				}
			});
		}
	}

	public static void runFactorio(String exeFile) throws IOException, InterruptedException {
		Process ps = Runtime.getRuntime().exec(new String[]{exeFile});
//		ps.waitFor();
//		java.io.InputStream is = ps.getInputStream();
//		byte b[] = new byte[is.available()];
//		is.read(b, 0, b.length);
	}
}
