/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz;

import novaz.tools.Log;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class ModPack {
	String[] directoriesUsed = {"mods", "saves"};
	String[] directoryLocations;
	private String name;

	public ModPack(String name) throws IOException {
		this.name = name;
		figureSourceDirs();
		createFolderStructure();
	}

	private void figureSourceDirs() {
		directoryLocations = new String[directoriesUsed.length];
		for (int i = 0; i < directoriesUsed.length; i++) {
			File source = new File(Config.gameLocation + "/" + directoriesUsed[i]);
			directoryLocations[i] = source.getAbsolutePath();
			//for windows
			File f = new File(System.getenv("appdata") + "/Factorio");//Only override if the appdata folder exists
			if (f.exists()) {
				directoryLocations[i] = f.getAbsolutePath() + "/" + directoriesUsed[i];
			}//other Os's here maybe?

		}
		Log.debug("DIRECTORY LOCATIONS");
		Log.debug(directoriesUsed);
		Log.debug(directoryLocations);
	}

	private void createFolderStructure() throws IOException {

		for (String directory : directoriesUsed) {
			File f = new File(Config.packLocation + "/" + name + "/" + directory);
			if (!f.exists()) {
				f.getParentFile().mkdirs();
				f.mkdir();
			}
//			f.getPath()
		}
	}

	public void updateSymbolicLinks() throws IOException {
		for (int i = 0; i < directoriesUsed.length; i++) {
			File link = new File(directoryLocations[i]);
			File target = new File(Config.packLocation + "/" + name + "/" + directoriesUsed[i]);
			if (link.exists()) {
				if (Files.isSymbolicLink(link.toPath())) {
					Files.delete(link.toPath().toRealPath(LinkOption.NOFOLLOW_LINKS));
				} else if (link.exists()) {
					File backupDirectory = new File(target.getAbsolutePath()+"_old");
					if(backupDirectory.exists()){
						FileUtils.deleteDirectory(backupDirectory);
					}
					FileUtils.moveDirectory(link, backupDirectory);
				}
			}
			//check for broken Symbolic link
			if (Files.exists(link.toPath(), LinkOption.NOFOLLOW_LINKS)){
				Files.delete(link.toPath().toRealPath(LinkOption.NOFOLLOW_LINKS));
			}
			Files.createSymbolicLink(link.toPath(), Paths.get(target.getAbsolutePath()));
		}
	}
}
