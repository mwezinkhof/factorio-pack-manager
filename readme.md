what?
=====
The beginnings of a launcher for Factorio which allows you to easily switch between different modpacks/instances

Config
======
Example manager.cfg
```
#!property
  game_location=C\:\\Program Files/factorio
  last_pack=test
```

After you launch it for the first time, it will generate a config file[manager.cfg]. In there you will find 2 variables, game_location needs to point to the factorio directory
and last_pack to name of your pack. if the pack doesn't exist it will create one relative from where the tool is called from [./pack/{packname}]

Additionally to manually changing the file every time you want to change packs, 
you could start the jar file with a pack=name, for instance 
  
```
#!java

java -jar factoriomanager.jar pack=hardcorepack
```



How?
====
It will create packs in the directory the tool is run from.

The directory structure will look like this:


```
#!
.
|-- packs
|    |-- Packname1
|    |    + mods
|    |    + saves
|    |-- Packname2
|    |    + mods
|    |    + saves
| manager.cfg
| factorio_manager-0.1.6.jar
```


What does the program do?

* It searches for the factorio.exe file in program files
* checks the %appdata%/factorio folder for the mod and saves folders
* * if the folders are symbolic links they will be deleted
* * if they are normal folders, they will be moved to the pack
* makes a symbolic link from the %appdata%/factorio/[mods+saves] to the /packs/{packname}/[mods+saves]
* launches the game